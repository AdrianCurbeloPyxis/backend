console.log("start-test");
var express = require('express');
var bodyParser = require('body-parser');
var usersFile = require('./resources/users.json');
var accountsFile = require('./resources/accounts.json');
var app = express();
var port = process.env.PORT || 3000;
const URI = '/api-uruguay/v1/';

//Parse the body element
app.use(bodyParser.json());

//Get users
app.get(URI + 'users',
  function(req, res){
    let queryParams = req.query;

    //Filter elements by queryParams.
    let usersFileFiltered = usersFile.filter(function(item) {
      for (var key in queryParams) {
        if (item[key] === undefined || item[key] != queryParams[key])
          return false;
      }
      return true;
    });
    res.send(usersFileFiltered);
  }
);

//Get accounts
app.get(URI + 'accounts',
  function(req, res){
    let queryParams = req.query;

    //Filter elements by queryParams.
    let accountsFileFiltered = accountsFile.filter(function(item) {
      for (var key in queryParams) {
        if (item[key] === undefined || item[key] != queryParams[key])
          return false;
      }
      return true;
    });
    res.send(accountsFileFiltered);
  }
);

//Get a user by id
app.get(URI + 'users/:id',
  function(req, res){
    let userId = req.params.id;

    //Lambda's search
    res.send(usersFile.find(u => u.id == userId));
  }
);

//Get an account by id
app.get(URI + 'accounts/:id',
  function(req, res){
    let accountId = req.params.id;

    //Lambda's search
    res.send(accountsFile.find(a => a.id == accountId));
  }
);

//Post a user
app.post(URI + 'users',
  function(req, res){
   //Showing the headers params
   console.log("Header:"+req.headers);

   //Modifying body and send it back.
   var userBody = req.body;
   req.body.id = usersFile.length + 1;
   res.send({"msg": req.body});
  }
);

//Put a user
app.put(URI + 'users/:id',
  function(req, res){
   let userId = req.params.id;
   let userBody = req.body;
   let user = usersFile.find(u => u.id == userId);
   let statuscode;

   if(user != undefined){
     var result = {};
     Object.keys(user).forEach(key => result[key] = user[key]);
     Object.keys(userBody).forEach(key => result[key] = userBody[key]);
     usersFile.forEach((element, index) => {
        if(element.id == userId){
            usersFile[index] = result;
        }
      });
     statuscode = 200;
   }else{
     statuscode = 400;
   }
   res.status(statuscode).send(usersFile);
  }
);

//Delete a user
app.delete(URI + 'users/:id',
  function(req, res){
    let userId = req.params.id;
    var index = usersFile.findIndex(function(item, i){
       return item.id == userId;
    });

    if ( index != -1 ) {
       usersFile.splice(index,1);
       res.status(200);
       res.send(usersFile);
    }
    res.status(404);
    let msgResponse = {'msg' : 'Update ERROR: Usuario no existe'};
    res.send(msgResponse);
  }
);

app.listen(port);
console.log("Escuchando en el puerto 3000...");
