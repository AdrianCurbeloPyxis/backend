
var express = require('express');
var app = express();
var requestJSON = require('request-json');

//Import paths
let paths = require('../const/paths.js');
const URI = paths.URI;
const apikeyMLab = paths.apikeyMLab;
const baseMLabURL = paths.baseMLabURL;

//Imports models
let users = require('../models/user.js');
let User = users.User;
