console.log("start-test");
var express = require('express');
var bodyParser = require('body-parser');
var requestJSON = require('request-json');
var app = express();
var port = process.env.PORT || 3000;

//Enable CORS
app.use(function(req, res, next) {
 res.header("Access-Control-Allow-Origin", "*");
 res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
 next();
});

//Imports controllers
/*
let controller_users = require('./controllers/users.js');
let accounts_users = require('./controllers/accounts.js');
let transactions_users = require('./controllers/transactions.js');
*/

//Imports models
let users = require('./models/user.js');
let User = users.User;
let accounts = require('./models/account.js');
let Account = accounts.Account;
let transactions = require('./models/transaction.js');
let Transaction = transactions.Transaction;

//Import paths
let paths = require('./const/paths.js');
const URI = paths.URI;
const apikeyMLab = paths.apikeyMLab;
const baseMLabURL = paths.baseMLabURL;
const apikeyExApi = paths.apikeyExApi;
const baseExApiURL = paths.baseExApiURL;

//Parse the body element
app.use(bodyParser.json());


// GET users consumiendo API REST de mLab
app.get(URI + 'users',
  function(req, res) {
    var httpClient = requestJSON.createClient(baseMLabURL);

    var queryString = 'f={"_id":0}&';
    httpClient.get('users?' + queryString + apikeyMLab,
      function(err, respuestaMLab, body) {
        var response = {};
        if(err) {
            response = {
              "msg": {
                  message: "Error obteniendo usuarios.",
                  message_type: "alert"
                }
            }
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {
              "msg": {
                  message: "Usuario no encontrado.",
                  message_type: "alert"
                }
            }
            res.status(404);
          }
        }
        res.send(response);
      });
});

// Petición GET con id en mLab
app.get(URI + 'users/:id',
  function (req, res) {
    var id = req.params.id;
    var queryString = 'q={"id":' + id + '}&';
    var queryStrField = 'f={"_id":0}&';
    var httpClient = requestJSON.createClient(baseMLabURL);
    httpClient.get('users?' + queryString + queryStrField + apikeyMLab,
      function(err, respuestaMLab, body){
        console.log("Respuesta mLab correcta.");
      //  var respuesta = body[0];
        var response = {};
        if(err) {
            response = {
              "msg": {
                  message: "Error obteniendo usuarios.",
                  message_type: "alert"
                }
            }
            res.status(500);
        } else {
          if(body.length > 0) {
            response = body;
          } else {
            response = {
              "msg": {
                  message: "Usuario no encontrado.",
                  message_type: "alert"
                }
            }
            res.status(404);
          }
        }
        res.send(response);
      });
});


// POST 'users' mLab
app.post(URI + 'users',
  function(req, res){
    var clienteMlab = requestJSON.createClient(baseMLabURL);
    clienteMlab.get('users?' + apikeyMLab,
      function(error, respuestaMLab, body){
        newID = body.length + 1;
        var newUser = {
          "id" : newID,
          "first_name" : req.body.first_name,
          "last_name" : req.body.last_name,
          "email" : req.body.email,
          "password" : req.body.password
        };
        clienteMlab.post(baseMLabURL + "users?" + apikeyMLab, newUser,
          function(error, respuestaMLab, body){
            res.send(body);
          });
      });
  });

  // Petición DELETE con id en mLab
  app.delete(URI + 'users/:id',
    function (req, res) {
      var id = req.params.id;
      var queryString = 'q={"id":' + id + '}&';
      var httpClient = requestJSON.createClient(baseMLabURL);

      httpClient.get('users?' + queryString + apikeyMLab,
        function(err, respuestaMLab, body){
          let response = body[0];

          //Llamo al delete de Mlab.
          httpClient.delete('users/' + response._id.$oid + '?' + apikeyMLab,
            function(err, respuestaMLab, body){
              var response = {};
              if(err) {
                  response = {
                    "msg": {
                        message: "Error borrando usuario.",
                        message_type: "alert"
                      }
                  }
                  res.status(500);
              } else {
                if(body.length > 0) {
                  response = body;
                } else {
                  response = {
                    "msg": {
                        message: "Usuario borrado correctamente.",
                        message_type: "success"
                      }
                  }
                  res.status(404);
                }
              }
              res.send(response);
            });
        });
  });

  // Petición PUT con id en mLab
  app.put(URI + 'users/:id',
    function (req, res) {
      var id = req.params.id;
      let userBody = req.body;
      var queryString = 'q={"id":' + id + '}&';
      var httpClient = requestJSON.createClient(baseMLabURL);

      httpClient.get('users?' + queryString + apikeyMLab,
        function(err, respuestaMLab, body){
          let response = body[0];

          //Actualizo los campos del usuario
          var updatedUser = {};
          Object.keys(response).forEach(key => updatedUser[key] = response[key]);
          Object.keys(userBody).forEach(key => updatedUser[key] = userBody[key]);

          //Llamo al put de mlab.
          httpClient.put('users/' + response._id.$oid + '?' + apikeyMLab, updatedUser,
            function(err, respuestaMLab, body){
              var response = {};
              if(err) {
                  response = {
                    "msg": {
                        message: "Error actualizando usuarios.",
                        message_type: "alert"
                      }
                  }
                  res.status(500);
              } else {
                if(body.length > 0) {
                  response = body;
                } else {
                  response = {
                    "msg": {
                        message: "Usuario actualizado correctamente.",
                        message_type: "sucess"
                      }
                  }
                  res.status(404);
                }
              }
              res.send(response);
            });
        });
  });

  //DELETE (usando el PUT)

  //PUT (usando q=id en lugar de oid.)

  //Method POST login
app.post(URI + "login",
  function (req, res){
    var email = req.body.email;
    var pass = req.body.password;
    var queryStringEmail = 'q={"email":"' + email + '"}&';
    var queryStringpass = 'q={"password":' + pass + '}&';
    var  clienteMlab = requestJSON.createClient(baseMLabURL);
    clienteMlab.get('users?'+ queryStringEmail + apikeyMLab ,
    function(error, respuestaMLab , body) {
      console.log("entro al body:" + body );
      var respuesta = body[0];
      console.log(respuesta);
      if(respuesta != undefined){
          if (respuesta.password == pass) {
            console.log("Login Correcto");
            var session = {"logged":true};
            var login = '{"$set":' + JSON.stringify(session) + '}';
            console.log(baseMLabURL+'?q={"id": ' + respuesta.id + '}&' + apikeyMLab);
            clienteMlab.put('users?q={"id": ' + respuesta.id + '}&' + apikeyMLab, JSON.parse(login),
              function(errorP, respuestaMLabP, bodyP) {
                response = {
                  "msg": {
                      message: "Login correcto.",
                      message_type: "success"
                    },
                  "authorization": true,
                  "id":respuesta.id
                }
                res.send(response);
              });
          }
          else {
            res.send({
              "msg": {
                  message: "Contraseña incorrecta.",
                  message_type: "alert"
                },
              "authorization": false});
          }
      } else {
        console.log("Email Incorrecto");
        res.send({"msg": {
                    message: "Email incorrecto.",
                    message_type: "alert"
                  },
                  "authorization": false});
      }
    });
});


//Method POST logout
app.post(URI + "logout",
  function (req, res){
    var email = req.body.email;
    var queryStringEmail = 'q={"email":"' + email + '"}&';
    var  clienteMlab = requestJSON.createClient(baseMLabURL);
    clienteMlab.get('users?'+ queryStringEmail + apikeyMLab ,
    function(error, respuestaMLab, body) {
      console.log("entro al get");
      var respuesta = body[0];
      console.log(queryStringEmail);
      console.log(body);
      console.log(respuesta);
      if(respuesta != undefined){
            console.log("logout Correcto");
            var session = {"logged":true};
            var logout = '{"$unset":' + JSON.stringify(session) + '}';
            console.log(logout);
            clienteMlab.put('users?q={"id": ' + respuesta.id + '}&' + apikeyMLab, JSON.parse(logout),
              function(errorP, respuestaMLabP, bodyP) {
                response = {
                  "msg": {
                      message: "Logout correcto.",
                      message_type: "alert"
                    },
                  "authorization": false
                }
                res.send(response);
              });
      } else {
        console.log("Error en logout");
        res.send({"msg": {
                    message: "Error en logout.",
                    message_type: "alert"
                  },
                  "authorization": true});
      }
    });
});

//Method POST register
app.post(URI + "register",
function (req, res){
  var email = req.body.email;
  var pass = req.body.password;
  var queryStringEmail = 'q={"email":"' + email + '"}&';
  var  clienteMlab = requestJSON.createClient(baseMLabURL);
  clienteMlab.get('users?'+ queryStringEmail + apikeyMLab ,
  function(error, respuestaMLab , body) {
    var respuesta = body[0];
    console.log(respuesta);
    if(respuesta != undefined){
      console.log("El usuario ya se encuentra registrado");
      res.send({"msg": {
                  message: "Usuario ya se encuentra registrado.",
                  message_type: "alert"
                },
                "authorization": false});
    } else {
      console.log("El email no existe, continuo con el registro");

      //Busco la cantidad de usuarios logueados.
      clienteMlab.get('users?'+ apikeyMLab ,
      function(error, respuestaMLab , body) {
        newID = body.length + 1;
        var newUser = {
          "id" : newID,
          "email" : req.body.email,
          "password" : req.body.password,
          "first_name" : req.body.firstname,
          "last_name" : req.body.lastname,
          "logged":true
        };
        clienteMlab.post(baseMLabURL + "users?" + apikeyMLab, newUser,
          function(error, respuestaMLab, body){
            console.log("El usuario se registró correctamente");
            res.send({"msg": {
                            message: "El usuario se registró correctamente.",
                            message_type: "success"
                            },
                      "authorization": true,
                      "id":newID});
          });
      });
    }
  });
});

// Petición GET a api externa
app.get(URI + 'currency',
  function (req, res) {
    var queryString = 'q=USD_UYU&compact=ultra';
    var httpClient = requestJSON.createClient(baseExApiURL);
    httpClient.get('convert?' + queryString ,
      function(err, respuestaExApi, body){
        console.log("Respuesta external api correcta.");
        var response = {};
        if(err) {
            response = {
              "msg": {
                  message: "Error obteniendo datos de moneda.",
                  message_type: "alert"
                }
            }
            res.status(500);
        } else {
            response = body;
        }
        res.send(response);
      });
});

// GET ACCOUNTS consumiendo API REST de mLab
app.get(URI + 'users/:id/accounts',
  function(req, res) {
    var httpClient = requestJSON.createClient(baseMLabURL);
    var id_usuario = req.params.id;

    var queryString = 'q={"id_usuario":'+id_usuario+'}&';
    httpClient.get('accounts?' + queryString + apikeyMLab,
      function(err, respuestaMLab, body) {
        var response = {};
        if(err) {
            response = {
              "msg": {
                  message: "Error obteniendo transacciones.",
                  message_type: "alert"
                }
            }
            res.status(500);
        } else {
          console.log(body);
          if(body.length > 0) {
            response = body;
          } else {
            response = {
              "msg": {
                  message: "Transacciones no encontradas.",
                  message_type: "alert"
                }
            }
            res.status(404);
          }
        }
        res.send(response);
      });
});

// GET ACCOUNT consumiendo API REST de mLab
app.get(URI + 'users/:id/accounts/:id_account',
  function(req, res) {
    var httpClient = requestJSON.createClient(baseMLabURL);
    var id_usuario = req.params.id;
    var id_account = req.params.id_account;

    var queryString = 'q={"id_usuario":'+id_usuario+',"id": '+id_account+'}&';
    httpClient.get('accounts?' + queryString + apikeyMLab,
      function(err, respuestaMLab, body) {
        var response = {};
        if(err) {
            response = {
              "msg": {
                  message: "Error obteniendo transacciones.",
                  message_type: "alert"
                }
            }
            res.status(500);
        } else {
          console.log(body);
          if(body.length > 0) {
            response = body;
          } else {
            response = {
              "msg": {
                  message: "Transacciones no encontradas.",
                  message_type: "alert"
                }
            }
            res.status(404);
          }
        }
        res.send(response);
      });
});

// GET TRANSACTIONS consumiendo API REST de mLab
app.get(URI + 'users/:id/accounts/:id_account/transactions',
  function(req, res) {
    var httpClient = requestJSON.createClient(baseMLabURL);
    var id_account = req.params.id_account;
    var transaction_type = req.query.transaction_type;
    var start_date = req.query.start_date;
    var end_date = req.query.end_date;

    var queryString = 'q={"id_cuenta":'+id_account+',';

    if(transaction_type != "" && transaction_type != "undefined" && transaction_type != undefined && transaction_type != "TODOS"){
      queryString += '"tipo":"'+transaction_type+'",';
    }
    
    if(start_date != undefined && start_date != ""){
      queryString += '"fecha": {"$gte": "'+start_date+'"';
        if(end_date == undefined || end_date == ""){
          queryString += "}";
        }
    }
    if(end_date != undefined && end_date != ""){
      if(start_date == undefined || start_date == ""){
        queryString += '"fecha": {';
      }else{
        queryString += ',';
      }
      queryString += '"$lte": "'+end_date+'"},';
    }
      queryString += '}&';

      console.log(queryString);

    httpClient.get('transactions?' + queryString + apikeyMLab,
      function(err, respuestaMLab, body) {
        var response = {};
        if(err) {
            response = {
              "msg": {
                  message: "Error obteniendo transacciones.",
                  message_type: "alert"
                }
            }
            res.status(500);
        } else {
          console.log(body);
          if(body.length > 0) {
            response = body;
          }
        }
        res.send(response);
      });
});

// GET TRANSACTION consumiendo API REST de mLab
app.get(URI + 'users/:id/accounts/:id_account/transactions/:id_transaction',
  function(req, res) {
    var httpClient = requestJSON.createClient(baseMLabURL);
    var id_account = req.params.id_account;
    var id_transaction = req.params.id_transaction;

    var queryString = 'q={"id_cuenta":'+id_account+',"id":'+id_transaction+'}&';
    httpClient.get('transactions?' + queryString + apikeyMLab,
      function(err, respuestaMLab, body) {
        var response = {};
        if(err) {
            response = {
              "msg": {
                  message: "Error obteniendo transacciones.",
                  message_type: "alert"
                }
            }
            res.status(500);
        } else {
          console.log(body);
          if(body.length > 0) {
            response = body;
          } else {
            response = {
              "msg": {
                  message: "Transacciones no encontradas.",
                  message_type: "alert"
                }
            }
            res.status(404);
          }
        }
        res.send(response);
      });
});

// POST 'transactions' mLab
app.post(URI + 'users/:id/accounts/:id_account/transactions',
  function(req, res){

    var clienteMlab = requestJSON.createClient(baseMLabURL);

    clienteMlab.get('transactions?' + apikeyMLab,
      function(error, respuestaMLab, body){
        var newID = body.length + 1;
        var newTransaction = {
          "id" : newID,
          "id_cuenta" : parseInt(req.params.id_account),
          "fecha" : req.body.fecha,
          "descripcion" : req.body.descripcion,
          "tipo" : req.body.tipo,
          "importe": parseFloat(req.body.importe)
        };

        clienteMlab.post(baseMLabURL + "transactions?" + apikeyMLab, newTransaction,
          function(error, respuestaMLab, body){
            var response = {};
            if(error) {
                response = {
                  "msg": {
                      message: "Error guardando transaccion.",
                      message_type: "alert"
                    }
                }
                console.log(error);
                res.status(500);
            } else {
              console.log(body);
              //Actualizo el saldo de la cuenta de la transaccion.
              actualizarSaldoCuenta(parseFloat(req.body.importe), parseInt(req.params.id_account));
                response = {
                  "msg": {
                      message: "Transacciones salvada correctamente.",
                      message_type: "success"
                    }
                }

            }
            res.send(response);

          });
      });
  });

  function actualizarSaldoCuenta(importe , id_cuenta){
    var queryString = 'q={"id":' + id_cuenta + '}&';
    var httpClient = requestJSON.createClient(baseMLabURL);

    httpClient.get('accounts?' + queryString + apikeyMLab,
      function(err, respuestaMLab, body){
        let response = body[0];
        console.log("RARA:"+JSON.stringify(response));
        var tempAccount = response;
        tempAccount.balance += importe;

        //Actualizo el importe de la cuenta.
        var updatedAccount = {};
        Object.keys(response).forEach(key => updatedAccount[key] = response[key]);
        Object.keys(tempAccount).forEach(key => updatedAccount[key] = tempAccount[key]);

        console.log("RERE:"+JSON.stringify(updatedAccount));
        //Llamo al put de mlab.
        httpClient.put('accounts/' + updatedAccount._id.$oid + '?' + apikeyMLab, updatedAccount,
          function(err, respuestaMLab, body){
              console.log("REIRI:"+JSON.stringify(body));
            if(err) {

            } else {

            }
          });
      });
  }



app.listen(port);
console.log("Escuchando en el puerto 3000...");
