#Imagen docker base
FROM node

#Crea directorio de trabajo de docker
WORKDIR /docker-api

#Copia archivos del proyecto al directorio de trabajo
ADD . /docker-api

#Instalo/sobreescribo las dependencias de node
RUN npm install

#Puerto donde exponemos el contenedor.
EXPOSE 3000

#Comando para lanzar la app
CMD ["npm","run","startprod"]
